# GND Import

This micro service makes some minor transformations to the GND items and appends the 
indexing metadata.

1. Extract key from from id field (will be used as _id in the index).
2. The date is inherited from the producer.
3. The rdf:types are expanded to their full namespaces.
4. The field gender is replaced with the wikidata gender values and field (wdt:P21).
5. Extracts dbo:birthYear and dbo:deathYear from the date fields where possible.
6. URL Decodes the Dbpedia URIs in the sameAs field. Otherwise they cannot be 
compared to the id-hub.
7. Adds indexing metadata for the consumer (index name is based on the 
[index_mapping.json](https://gitlab.com/swissbib/linked/workflows/blob/master/gnd/transformer/index_mapping.json)).

